# 02h20m_PLURALSIGHT_Kubernetes for Developers Moving from Docker Compose to Kubernetes Dan Wahlin 2020
## Docker-Compose Review
![DockerComposeReview](images/DockerComposeReview.png)

Q: Why would you move from docker-compose to kubernetes ?  
A:
Kubernetes can:
* scale container across multiple nodes (machine)
* provide container monitoring and healing
* offer robust deployment options
* and much more

## Kubernetes Review
![KubernetesResources](images/KubernetesResources.png)

![KubernetesYamlExample](images/KubernetesYamlExample.png)

# ==================================================
# 01_Kubernetes for Developers Core Concepts 2021 Dan Wahlin
## Kubernetes Key Features

![KubernetesFeatures](images/KubernetesFeatures.png)

## Kubernetes Building Blocks

![KubernetesBuildingBlocks](images/KubernetesBuildingBlocks.png)

## Kubernetes Components
![KubernetesComponents](images/KubernetesComponents.png)
### Control Plane
* Responsible for scheduling pods
* Detect and respond to cluster events
### Kube API Server
* Expose the Kubernetes API
### Etcd
* a database for all cluster data
### Kube Scheduler
* Watch for newly created pod and select which node to run them
### Kube Controller Manager
* Run controller processes (Node, Job, Entpoint, Service)
### Cloud Controller Manager
#### Node
#### Kubelet
* An agent that make sure that container are running in a pod.
#### kube-proxy
* Maintain network rules on nodes.
#### Container runtime
* Software that is responsible for running containers (Docker, ContainerD, CRI-O)

Reference:
https://kubernetes.io/docs/concepts/overview/components/

## Kubernetes Nodes

![KubernetesNodes](images/KubernetesNodes.png)

## How to interact with Kubernetes

![KubectlToKubernetes](images/KubectlToKubernetes.png)

**For more info, see the official documentation:**
https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-strong-getting-started-strong-

Q: Pod vs Container ?  
A:
* Pod are the smallest object of the kubernetes object model. It is used to run container(s). Mostly, 1 container per pod.
* Pod has: IP address, memory, volume, etc...that are shared between its container(s).
* Container processes need to bind to different ports within a pod.

Q: Pod vs Node ?  
A:
* Pod cannot span nodes

![PodVsNode01](images/PodVsNode01.png)

Q: How to schedule a Pod ?  
A:
1. kubectl run &lt;PodName&gt; --image=&lt;ImageName&gt;  
Example: kubectl run my-nginx-server --image=nginx:latest
2. kubectl [create || apply] path/to/yaml/file 

Q: How to expose a Pod port to the external world ?  
A: 
kubectl port-forward &lt;PodName&gt; &lt;ExternalPort&gt;:&lt;ContainerPort&gt;  
Example: kubectl port-forward my-nginx-server 8080:80
Then open a web browser and visit "localhost:8080" to see the Nginx Webpage

Q: How to kill a running pod ?    
A: kubectl delete pod &lt;PodName&gt;

Q: How to safely delete a pod ?  
A: kubectl delete deployment &lt;DeploymentName&gt;

## Kubectl Pod Commands Summary
![WorkingWithPod](images/WorkingWithPod.png)

Q: How to run Kubernetes locally ?  
A:
1. Minikube
2. Docker Desktop + Kubernetes
3. KinD (Kubernetes in Docker)
4. Kubeadm

## Kubectl Commands Summary
![KubectlCommands](images/KubectlCommands.png)

## Kubernetes UI Dashboard
![KubernetesUIDashboard](images/KubernetesUIDashboard.png)
Reference:
https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/

## Introduction to YAML
![IntroductionToYAML](images/IntroductionToYAML.png)

## The 'Declarative' Way
![CreatePodViaYAMLExample](images/CreatePodViaYAMLExample.png)

## Performe a trial create and also validate the YAML
kubectl create -f path/to/yaml/file --dry-run --validate=true

### Create a pod from YAML
### Will cause an error if pod already exists
kubectl create -f path/to-yaml/file

### Alternate way to create or apply changes to a pod from YAML
kubectl apply -f path/to/yaml/file

Q: kubectl create vs kubectl apply ?  
A:
![CreateVsApply](images/CreateVsApply.png)
To demonstrate:
kubectl create -f path/to/yaml/file --save-config
kubectl get pod &lt;PodName&gt; -o [json || yaml]

Q: How to delete a pod ran using create/apply ?  
A:
kubectl delete -f path/to/yaml/file  
OR  
kubectl delete pod &lt;PodName&gt;

Q: How can I get more info about a kubernetes object ?  
A: kubectl describe &lt;KubernetesObjectName&gt;

Q: How can I attach to a pod ?  
A: kubectl exec &lt;PodName&gt; -it sh

## YAML Pod Commands Summary
![CreatingAndInspectingPodWithKubectl](images/CreatingAndInspectingPodWithKubectl.png)

## Pod Health
Q: What does Kubernetes rely on for Pod health ?  
A: Probes

Q: What are the different types of probe ?  
A:
1. Liveness probe: uses to determine the health of the pod
2. Readiness probe: uses to determine if the pod can receive requests

Q: What are the different return values of a probe ?  
A:
1. Success
2. failure
3. Unknown

![DefineHTTPLivenessProbe](images/DefineHTTPLivenessProbe.png)

![DefineExecActionLivenessProbe](images/DefineExecActionLivenessProbe.png)

![DefineReadinessProbe](images/DefineReadinessProbe.png)

## Kubernetes Deployment
Q: What is a replicaSet ?  
A: A declarative way to manage pods.

Q: What is a deployment ?  
A: A declarative way to manage pods using a ReplicaSet.

Q: What are the roles of the ReplicaSet ?  
A:
![RoleOfReplicaSet](images/RoleOfReplicaSet.png)

Q: What are the roles of the Deployment ?  
A:
![RoleOfDeployment](images/RoleOfDeployment.png)

### Simple Deployment
![SimpleDeploymentYaml](images/SimpleDeploymentYaml.png)

### Detailed Deployment
![DetailedDeployment](images/DetailedDeployment.png)

Q: How to launch/run a deployment ?  
A: kubectl [create || apply] --filename path/to/deplyoment/yaml/file

Q: How to view running deployments ?  
A: kubectl get deployments

Q: How to list deployment with their labels ?  
A: kubectl get deployments --show-labels

Q: How to get all deployments filter by label ?  
A: kubectl get deployments -l app=nginx

Q: How to delete a deployment ?  
A:
1. kubectl delete deployment [DeploymentName]
2. kubectl delete --filename path/to/yaml/file

Q: How to scale a deployment ?  
A:
1. kubectl scale deployment [DeploymentName] --replicas=[NumberOfReplicas]
2. kubectl scale --filename path/to/yaml/file --replicas=[NumberOfReplicas]

Q: How to view the status of a deployment ?  
A: kubectl describe deployment [DeploymentName]

## Service
Q: What is a service in kubernetes ?  
A: A service provide a single point of entry for accessing one or more pods.

Q: Why would I use a service instead of a pod directly via its IP address ?  
A: Since pod live and die, we cannot rely only on their IP address.
Also, pod can be scale up or scale down, IP address changes.

Q: What are the roles of a service ?  
A:
![RoleOfService](images/RoleOfService.png)

* Service act as a pod load balancer:

![HowServiceActInFrontOfPods](images/HowServiceActInFrontOfPods.png)

Q: What are the different service types ?  
A:
1. ClusterIP

![ClusterIPService](images/ClusterIPService.png)

2. NodePort

![NodePortService](images/NodePortService.png)

3. Loadbalancer

![LoadBalancerService](images/LoadBalancerService.png)

4. ExternalName

![ExternalNameService](images/ExternalNameService.png)

Summary:

![DifferentTypeOfService](images/DifferentTypeOfService.png)

### Port-Forwarding
Q: How can you access a pod from outside of Kubernetes ?  
A:
1. kubectl port-forward pod/[PodName] 8080:80  
Listen on port 8080 locally and forward to port 80 in pod

2. kubectl port-forward deployment/[DeploymentName] 8080:80  
Listen on port 8080 locally and forward to deployment's pod

3. kubectl port-forward service/[ServiceName] 8080  
Listen on port 8080 locally and forward to service's pod

### Service YAML
#### Overview
![ServiceYamlOverview](images/ServiceYamlOverview.png)
#### Concrete Example
![ConcreteServiceYaml](images/ConcreteServiceYaml.png)

![CreateNodePortService](images/CreateNodePortService.png)

![CreateLoadBalancerService](images/CreateLoadBalancerService.png)

![CreateExternalNameService](images/CreateExternalNameService.png)

Q: How do you deploy a service via a YAML file ?  
A:
1. kubectl create --filename path/to/service/yaml/file
2. kubectl apply --filename path/to/service/yaml/file

Q: How do you update a deployed service via a YAML file ?  
A: kubectl apply --filename path/to/service/yaml/file

Q: How do you delete a deployed service via a YAML file ?  
A: kubectl delete --filename path/to/service/yaml/file

Q: How to quickly test a service and pod with curl ?  
A: 
1. kubectl exec [PodName] -- curl -s http://[PodIP]
2. kubectl exec [PodName] -it sh
follow by:
apk add curl
follow by:
curl -s http://[PodIP]

## Storage
Q: What are the different types of storage ?  
A:
![DifferentTypeOfStorage](images/DifferentTypeOfStorage.png)

### Volume Types
![DifferentTypeOfVolume](images/DifferentTypeOfVolume.png)
#### Empty Directory Volume
![EmptyDirectoryVolumeYaml](images/EmptyDirectoryVolumeYaml.png)
An emptyDir volume is first created when a Pod is assigned to a node, and exists as long as that Pod is running on that node.
As the name says, the emptyDir volume is initially empty.
All containers in the Pod can read and write the same files in the emptyDir volume, though that volume can be mounted at the same or different paths in each container.
When a Pod is removed from a node for any reason, the data in the emptyDir is deleted permanently.  
Reference: https://kubernetes.io/docs/concepts/storage/volumes/#emptydir
#### HostPath Volume
![HostPathVolumeYaml](images/HostPathVolumeYaml.png)
A hostPath volume mounts a file or directory from the host node's filesystem into your Pod.
This is not something that most Pods will need, but it offers a powerful escape hatch for some applications.  
Reference: https://kubernetes.io/docs/concepts/storage/volumes/#hostpath
#### ConfigMap/Secret
* A ConfigMap provides a way to inject configuration data into pods.
The data stored in a ConfigMap can be referenced in a volume of type configMap and then consumed by containerized applications running in a pod.  
Reference: https://kubernetes.io/docs/concepts/storage/volumes/#configmap

* A secret volume is used to pass sensitive information, such as passwords, to Pods.
You can store secrets in the Kubernetes API and mount them as files for use by pods without coupling to Kubernetes directly. secret volumes are backed by tmpfs (a RAM-backed filesystem) so they are never written to non-volatile storage.  
Reference: https://kubernetes.io/docs/concepts/storage/volumes/#secret
### PersistentVolume
![PersistentVolume](images/PersistentVolume.png)

![PersistentVolumeSummary](images/PersistentVolumeSummary.png)
### PersistentVolumeClaim
![PersistentVolumeClaim](images/PersistentVolumeClaim.png)
A persistentVolumeClaim volume is used to mount a PersistentVolume into a Pod.
PersistentVolumeClaims are a way for users to "claim" durable storage (such as a GCE PersistentDisk or an iSCSI volume) without knowing the details of the particular cloud environment.  
Reference: https://kubernetes.io/docs/concepts/storage/volumes/#persistentvolumeclaim
### StorageClasses
![StorageClass](images/StorageClass.png)

![StorageClassSummary](images/StorageClassSummary.png)

## ConfigMap
![ConfigMapSummary](images/ConfigMapSummary.png)

![ConfigMapForPod](images/ConfigMapForPod.png)

![ConfigMapManifestExample](images/ConfigMapManifestExample.png)

![ConfigMapAsKeyValuePair](images/ConfigMapAsKeyValuePair.png)

![ConfigMapAsEnvFile](images/ConfigMapAsEnvFile.png)

![CreatingConfigMap](images/CreatingConfigMap.png)

## Secret
![SecretSummary](images/SecretSummary.png)

![CreatingSecret](images/CreatingSecret.png)
# ==================================================
# Useful Commands to know
## Start from a clean state
```shell
# source: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#delete
kubectl get all
kubectl delete pods --all
kubectl delete services --all
kubectl delete deployments --all
kubectl get all
```

## Run a single pod
```shell
# source: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#run
# kubectl run [PodName] --image=[ImageName]
kubectl run my-nginx-server --image=nginx
```

## Describe a Pod
```shell
kubectl describe pod [PodName]
```

## Get Pod YAML
```shell
kubectl get pod [PodName] -o json
# OR
kubectl get pod [PodName] -o yaml
```

## Port-Forward (to allow container to see the outside world)
```shell
# Forward local port (Host machine) to pod
# source: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#port-forward
kubectl port-forward my-nginx-server 8080:80
```

## Execute a command in a container
```shell
# source: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#exec
kubectl exec my-nginx-server -it bash
```

## Scale a deployment to 4 replicas
```shell
# source: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#scale
kubectl apply --filename simple_deployment_nginx
kubectl get all
kubectl scale --replicas=4 --filename simple_deployment_nginx.yml
kubectl get all
# NOTE: You cannot scale a single pod. Only a replica-set/deployment
```

## List of Kubernetes objects
```shell
# source: https://github.com/kubernetes/kubectl/issues/837#issuecomment-632092853
kubectl api-resources
```

# ==================================================
# DevOps Overview
![DevOps_Tools_01](images/DevOps_Tools_01.png)

![DevOps_Tools_02](images/DevOps_Tools_02.png)

![DevOps_Tools_03](images/DevOps_Tools_03.jpg)
