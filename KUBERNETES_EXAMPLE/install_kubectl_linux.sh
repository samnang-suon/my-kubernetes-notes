###################################################################################################
# KUBECTL
###################################################################################################
# source:
# https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-using-native-package-management

# Update apt package index
sudo apt-get update

# Install needed packages
sudo apt-get install -y apt-transport-https ca-certificates curl

# Download the Google Cloud public signing key
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

# Add the Kubernetes apt repository
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

# Update apt package index with the new repository and install kubectl:
sudo apt-get update && sudo apt-get install -y kubectl

# Verify kubectl is installed
kubectl version


###################################################################################################
# VIRTUALBOX
###################################################################################################
# Install Virtualbox
# source: https://phoenixnap.com/kb/install-virtualbox-on-ubuntu
sudo apt-get update && sudo apt-get install virtualbox --yes


###################################################################################################
# VMWARE
###################################################################################################
![VMwareVirtualization](images/VMwareVirtualization.png)
# Otherwise, you will get:
# StartHost failed, but will try again: creating host: create: precreate: This computer doesn't have VT-X/AMD-v enabled. Enabling it in the BIOS is mandatory


###################################################################################################
# MINIKUBE
###################################################################################################
# Install Minikube
# source: https://minikube.sigs.k8s.io/docs/start/
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb

# Verify minikube is installed
minikube version

# Start a cluster using the docker driver
# source: https://minikube.sigs.k8s.io/docs/drivers/docker/
minikube start --driver=virtualbox
# NOTE: You computer needs to have at least 2 CPU, Otherwise, you will get:
# Exiting due to RSRC_INSUFFICIENT_CORES: Requested cpu count 2 is greater than the available cpus of 1
# NOTE: For some reason, you cannot use docker driver. Otherwise, you will get:
# Exiting due to DRV_AS_ROOT: The "docker" driver should not be used with root privileges.
# NOTE: For some reason, you cannot use 'sudo' with Virtualbox. Otherwise, you will get:
# Exiting due to DRV_AS_ROOT: The "virtualbox" driver should not be used with root privileges.

# Verify minikube is running
minikube status
# Example
# minikube
# type: Control Plane
# host: Running
# kubelet: Running
# apiserver: Running
# kubeconfig: Configured
