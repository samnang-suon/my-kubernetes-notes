# Verify Minikube is running
minikube status

# Start from clean state
# source: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#delete
kubectl get all
kubectl delete pods --all
kubectl delete services --all
kubectl delete deployments --all
kubectl get all


# Create a pod
# source: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#run
# kubectl run [PodName] --image=[ImageName]
kubectl run my-nginx-server --image=nginx


# Forward local port to pod
# source: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#port-forward
kubectl port-forward my-nginx-server 8080:80


# Execute a command in a container
# source: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#exec
kubectl exec my-nginx-server -it bash


# Scale a deployment to 4 replicas
# source: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#scale
kubectl apply --filename simple_deployment_nginx
kubectl get all
kubectl scale --replicas=4 --filename simple_deployment_nginx.yml
kubectl get all
# NOTE: You cannot scale a single pod. Only a replica-set/deployment