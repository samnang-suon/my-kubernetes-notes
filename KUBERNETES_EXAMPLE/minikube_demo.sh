# Verify that Minikube is installed
minikube status
minikube start --driver=virtualbox

# Verify that kubectl is installed
kubectl version

# See cluster status
kubectl get all
# Delete extra pod/service/deployment and make sure to only have:
# NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
# service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   4h11m

# source:
# https://minikube.sigs.k8s.io/docs/start/
###################################################################################################
# NodePort Service Demo
###################################################################################################
## Create a sample deployment and expose it on port 8080:
kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.4
kubectl expose deployment hello-minikube --type=NodePort --port=8080

## 
kubectl get services hello-minikube

## Use port-forward
kubectl port-forward service/hello-minikube 7080:8080

## then open a web browser and visit http://localhost:7080/


###################################################################################################
# LoadBalancer Service Demo
###################################################################################################
# Before running the following, make sure to delete previous deployment:
kubectl delete deployment hello-minikube
kubectl delete service hello-minikube
# To access a LoadBalancer deployment, use the “minikube tunnel” command. Here is an example deployment:
kubectl create deployment balanced --image=k8s.gcr.io/echoserver:1.4
kubectl expose deployment balanced --type=LoadBalancer --port=8080
minikube tunnel

# To find the routable IP, run this command and examine the EXTERNAL-IP column:
kubectl get services balanced
# Your deployment is now available at <EXTERNAL-IP>:9090


###################################################################################################
# MINIKUBE LIFE-CYCLE
###################################################################################################
# Pause Kubernetes without impacting deployed applications:
minikube pause

# Halt the cluster
minikube stop

# Delete all of the minikube clusters
minikube delete --all